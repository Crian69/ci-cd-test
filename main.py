from flask import Flask

app = Flask(__name__)

@app.get("/")
def home():
    return "Hello World"

@app.get("/<stuff>")
def stuff(stuff):
    return stuff

if __name__ == "__main__":
    app.run(debug=True)